package com.parcial2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.*

class Order : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_order)
        val spn2: Spinner =findViewById(R.id.spinner2)
        val proteine=resources.getStringArray(R.array.Proteine)
        val adaptador1= ArrayAdapter(this,android.R.layout.simple_spinner_item,proteine)
        spn2.adapter=adaptador1
        var OrderVeggie:String="Veggtables:"
        val veggieOnion:CheckBox=findViewById(R.id.chVeggie1)
        val veggieTomatoes:CheckBox=findViewById(R.id.chVeggie2)
        val veggieLetuice:CheckBox=findViewById(R.id.chVeggie3)
        val veggiePickles:CheckBox=findViewById(R.id.chVeggie4)
        val veggieNoOne:CheckBox=findViewById(R.id.chVeggie5)
        val btnOrder: Button = findViewById(R.id.btnOrder)
        btnOrder.setOnClickListener{ Toast.makeText( this, "$OrderVeggie", Toast.LENGTH_SHORT).show()}
        veggieNoOne.setOnClickListener {
            if(veggieNoOne.isChecked){
                veggieOnion.setEnabled(false)
                veggieTomatoes.setEnabled(false)
                veggieLetuice.setEnabled(false)
                veggiePickles.setEnabled(false)
                OrderVeggie="$OrderVeggie No One"
            }else{
                if(veggieOnion.isChecked){
                    OrderVeggie="$OrderVeggie Onion,"
                }
                if(veggieTomatoes.isChecked){
                    OrderVeggie="$OrderVeggie Tomatoes,"
                }
                if(veggieLetuice.isChecked){
                    OrderVeggie="$OrderVeggie Letuice,"
                }
                if(veggiePickles.isChecked){
                    OrderVeggie="$OrderVeggie Pickles,"
                }
            }
        }
    }
}