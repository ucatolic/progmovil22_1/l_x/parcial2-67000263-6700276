package com.parcial2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.view.Menu
import android.view.MenuItem
import android.widget.*

class MainActivity : AppCompatActivity() {



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
        val btnSave: Button = findViewById(R.id.btnSave)
        btnSave.setOnClickListener{ checkName()}
        val spn1: Spinner =findViewById(R.id.spinner)
        val mobile=resources.getStringArray(R.array.Mobiles)
        val adaptador=ArrayAdapter(this,android.R.layout.simple_spinner_item,mobile)
        spn1.adapter=adaptador
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main,menu)
        return true
    }
    override fun onOptionsItemSelected(item:MenuItem)= when (item.itemId){
        R.id.developers_menu->{
         val intent= Intent(this, InfoDev::class.java )
         startActivity(intent)
         true
        }
        R.id.Client_menu->{
            val etName: EditText = findViewById(R.id.Name)
            val Name: String = etName.text.toString()
            val etPhone: EditText = findViewById(R.id.Phone)
            val Phone: String = etPhone.text.toString()
            val etAddress: EditText = findViewById(R.id.Address)
            val Address: String = etAddress.text.toString()
            val etEmail: EditText = findViewById(R.id.email)
            val Email: String = etEmail.text.toString()
            var Message:String="Cliente registrado Nombre:$Name, Numero telefonico:$Phone, Dirección: $Address, Email:$Email"
            if(etName.text.isNotEmpty() and etPhone.text.isNotEmpty() and etAddress.text.isNotEmpty() and etEmail.text.isNotEmpty() ){
                val intent= Intent(this, InfoClient::class.java )
                intent.putExtra("Intent_Message",Message)
                startActivity(intent)
                true
            }else{
                errorMsg()
                Message="El cliente aún no ha registrado datos"
                val intent= Intent(this, InfoClient::class.java )
                intent.putExtra("Intent_Message",Message)
                startActivity(intent)
                true
            }


        }
        R.id.AddBurguer->{
            val etName: EditText = findViewById(R.id.Name)
            val etPhone: EditText = findViewById(R.id.Phone)
            val etAddress: EditText = findViewById(R.id.Address)
            val etEmail: EditText = findViewById(R.id.email)
            if(etName.text.isNotEmpty() and etPhone.text.isNotEmpty() and etAddress.text.isNotEmpty() and etEmail.text.isNotEmpty() ){
                val intent= Intent(this, Order::class.java )
                startActivity(intent)
                true
            }else{
                errorMsgAdd()
                true
            }
        }
        else -> super.onOptionsItemSelected(item)
    }

    fun checkName(){
        val etName: EditText = findViewById(R.id.Name)
        val etPhone: EditText = findViewById(R.id.Phone)
        val etAddress: EditText = findViewById(R.id.Address)
        val etEmail: EditText = findViewById(R.id.email)
        if(etName.text.isNotEmpty() and etPhone.text.isNotEmpty() and etAddress.text.isNotEmpty() and etEmail.text.isNotEmpty() ){
            doneMsg()
        }else{
            errorMsg()
        }
    }
    fun errorMsg(){
        Toast.makeText( this, "Error, Alguno de los campos no fue diligenciado", Toast.LENGTH_SHORT).show()
    }
    fun doneMsg(){
        Toast.makeText( this, "Los datos fueron guardados con exito", Toast.LENGTH_SHORT).show()
    }
    fun errorMsgAdd(){
        Toast.makeText( this, "Para habilitar esta opción por favor diligencie los datos requeridos ", Toast.LENGTH_SHORT).show()
    }

}