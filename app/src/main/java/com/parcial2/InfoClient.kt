package com.parcial2

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class InfoClient: AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.info_client)
        showInfo()
    }
    fun showInfo(){
        val bundle=intent.extras
        val message = bundle?.get("Intent_Message")
        val ShowMsg: TextView=findViewById(R.id.infoClient)
        ShowMsg.text="$message"
    }
}